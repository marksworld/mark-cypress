import {ToDoList} from "../../src/page-objects/google-search.page";
const toDo = new ToDoList();

//expect assertion

describe('Cypress To-Do List navigation', () => {
    beforeEach(() => {
        cy.visit('https://example.cypress.io/todo')
    })

    it('Adds a to-do item', () => {
        toDo.toDoListField().type("Something");
        toDo.enter();
        toDo.getLastToDoInList().should('have.text', 'Something');
        
    });

    it('Checks accessibility of the page', () => {
        
        cy.injectAxe();
    })
});

