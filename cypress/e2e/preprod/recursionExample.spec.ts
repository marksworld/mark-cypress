let remainingReloadAttempts = 3;

function checkFailedDrivingStatus(){
    cy.wait(1000).then(() => {
       cy.get('EXAMPLE').invoke('text').as('drivingHistoryResult')
    })
    cy.get<string>('@drivingHistoryResult').then((result) => {
    cy.log(result)
      if(--remainingReloadAttempts){
        if(result == "failed"){
          return
        } else {
          cy.wait(1000).then(() => {
            cy.reload();
            return checkFailedDrivingStatus();
          })
        }
      } else {
        throw Error('Driving History status expecting '+ "failed" + ' but got '+ result);
      }
    })
}
checkFailedDrivingStatus()  
