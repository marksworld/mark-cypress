/// <reference types="cypress" />
 
export class ToDoList{
    toDoListField(){
    return cy.get('.new-todo');
    }
    enter(){
     return cy.get('.new-todo').type("{enter}");
    }
    getLastToDoInList(){
    return cy.get('.todo-list li').last();
    }
  }